/*
 * Copyright 2013-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* Analysis **************************************************************** */

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-feature',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-feature-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-out-item',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-out-item-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-project-contraint',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-project-contraint-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-quality-scenario',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-quality-scenario-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-quality',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-quality-target',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-requirement',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-requirement-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-user-character',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-user-character-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-vision-statement',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-vision-statement-type',
		PROJECTDOC.standardWizard);


/* Deployment ************************************************************** */

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-artifact',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-artifact-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence:create-doctype-template-environment',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence:create-doctype-template-environment-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-node',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-node-type',
		PROJECTDOC.standardWizard);


/* Design ****************************************************************** */

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-architecture-alternative',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-architecture-aspect',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-architecture-decision',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-architecture-decision-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-component',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-component-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-technical-debt',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-technical-debt-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-use-case',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-use-case-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-view',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-view-type',
		PROJECTDOC.standardWizard);


/* Implementation ********************************************************** */

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-code',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-code-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-property',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-property-type',
		PROJECTDOC.standardWizard);


/* Test ******************************************************************** */

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-test-case',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-test-case-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-test-charter',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-test-charter-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-test-data',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-test-data-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-test-report',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-test-report-type',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-test-session',
		PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-swdev:create-doctype-template-test-session-type',
		PROJECTDOC.standardWizard);